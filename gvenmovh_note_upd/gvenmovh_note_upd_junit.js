/**
 * Since: 16-07-2021
 *
 *  Verificar TRIGGER :  gvenmovh_note_upd
 *  Trigger update gvenmovh_note
 * 
 *  Test positivo:
 *      - Foto : gvenmovh_note
 *      - Ejecución y foto al finalizar ejecución: 
 *        gvenmovh_note
 * 
 *  Test negativo:
 *       No hay test a realizar
 */

 const mArrTests = [
    {
        test_name : 'Test 1: Actualizar nota de cabecera expedicion de ventas',
        cabid     : 660,
        linid     : null,
        tipnot    : 'AV',
        orden     :  0,
        data      : 'Notas cabecera actualizada'
    }
];

const mArrTestsError = [
];

for (let mRowTest of mArrTests){
    
    // Verificar si el linid llega como nulo
    mRowTest.linid = mRowTest.linid == null ? 'IS NULL' : `= ${mRowTest.linid}`;
    
    test(`Initial snapshot. ${mRowTest.test_name}`, (test_unit) => {

        Ax.jsunit.assertResultSet(test_unit.snapshot, [
            {
                tableName: 'gvenmovh_note',
                columns  : `cabid, linid, tipnot, orden, data`,
                where    : `cabid  = ${mRowTest.cabid}    AND
                            linid    ${mRowTest.linid}    AND
                            tipnot = '${mRowTest.tipnot}' AND
                            orden  = ${mRowTest.orden}`
            }
        ]);
    });

    test(`Execute process. ${mRowTest.test_name}`, (test_unit) => {

        Ax.db.update("gvenmovh_note", 
            {
                data: mRowTest.data
            },` cabid  = ${mRowTest.cabid}  AND
                linid    ${mRowTest.linid}  AND
                tipnot = '${mRowTest.tipnot}' AND
                orden  = ${mRowTest.orden}`
        );

        Ax.jsunit.assertResultSet(test_unit.snapshot, [
            {
                tableName: 'gvenmovh_note',
                columns  : `cabid, linid, tipnot, orden, data`,
                where    : `cabid  = ${mRowTest.cabid}  AND
                            linid    ${mRowTest.linid}  AND
                            tipnot = '${mRowTest.tipnot}' AND
                            orden  = ${mRowTest.orden}`
            }
        ]);
    });

 }
 /**
 * Test Negativo:
 */
for(let mRowTestError of mArrTestsError) {
    
    mRowTestError.linid = mRowTestError.linid == null ? 'IS NULL' : `= ${mRowTestError.linid}`;
    
    test(`Expected errors ${mRowTestError.test_name}`, (test_unit) => {
        /**
        * Try, may throw an error for all cases in mSqlCondErr
        */
        
       Ax.jsunit.assertThrows('ERROR', () => {
            try {
                Ax.db.update("gvenmovh_note", 
                    {
                        data: mRowTestError.data
                    },`
                        cabid  =  ${mRowTestError.cabid}  AND
                        linid     ${mRowTestError.linid}  AND
                        tipnot = '${mRowTestError.tipnot}' AND
                        orden  =  ${mRowTestError.orden}
                    `
                );
            }
            catch(e) {
                Ax.jsunit.assertEquals(test_unit.snapshot, e.message);
                throw new Error(`ERROR`);
           }
       });
   });
}