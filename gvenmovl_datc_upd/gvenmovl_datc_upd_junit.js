/**
 * Since: 15-07-2021
 *
 *  Verificar TRIGGER :  gvenmovl_datc_upd
 *  Trigger delete for gvenmovl_datc
 * 
 *  Test positivo:
 *      - Foto : gvenmovl_datc, gvenmovl
 *      - Ejecución y foto al finalizar ejecución: 
 *        gvenmovl_datc, gvenmovl
 * 
 *  Test negativo:
 *       No hay test a realizar
 */

const mArrTests = [
    {
        test_name : 'Test 1: Actualizar dato contable de la linea 3204 de la expedición',
        linid     : 3204,
        proyec    : '09'
    }
];

const mArrTestsError = [
];

for (let mRowTest of mArrTests){

    test(`Initial snapshot. ${mRowTest.test_name}`, (test_unit) => {

        Ax.jsunit.assertResultSet(test_unit.snapshot, [
            {
                tableName: 'gvenmovl_datc',
                columns  : `proyec, seccio, ctacon, ctaaux, ctaexp,
                            centro, porcen, import, codpre, codpar`,
                where    : `gvenmovl_datc.linid = ${mRowTest.linid}`
            },
            {
                tableName: 'gvenmovl',
                columns  : `orden, codart, varstk, desvar, numlot, canmov, canalt, udmven, udmalt,
                            estlin, errlin, wkflin, impcos, cosmed, terdep, ubiori, ubides, precio, 
                            dtolin, pretar, dtotar, canabo, canfac`,
                where    : `gvenmovl.linid = ${mRowTest.linid}`
            }
        ]);
    });

    test(`Execute process. ${mRowTest.test_name}`, (test_unit) => {

        Ax.db.update("gvenmovl_datc", 
            {
                proyec: mRowTest.proyec
            },
            {
                linid : mRowTest.linid
            }
        );

        Ax.jsunit.assertResultSet(test_unit.snapshot, [
            {
                tableName: 'gvenmovl_datc',
                columns  : `proyec, seccio, ctacon, ctaaux, ctaexp,
                            centro, porcen, import, codpre, codpar`,
                where    : `gvenmovl_datc.linid = ${mRowTest.linid}`
            },
            {
                tableName: 'gvenmovl',
                columns  : `orden, codart, varstk, desvar, numlot, canmov, canalt, udmven, udmalt,
                            estlin, errlin, wkflin, impcos, cosmed, terdep, ubiori, ubides, precio, 
                            dtolin, pretar, dtotar, canabo, canfac`,
                where    : `gvenmovl.linid = ${mRowTest.linid}`
            }
        ]);
    });

 }
 /**
 * Test Negativo:
 */
for(let mRowTestError of mArrTestsError) {

    test(`Expected errors ${mRowTestError.test_name}`, (test_unit) => {
        /**
        * Try, may throw an error for all cases in mSqlCondErr
        */
       Ax.jsunit.assertThrows('ERROR', () => {
            try {
                Ax.db.update("gvenmovl_datc", 
                    {
                        proyec: mRowTest.proyec
                    },
                    {
                        linid : mRowTest.linid
                    }
                );
            }
            catch(e) {
                Ax.jsunit.assertEquals(test_unit.snapshot, e.message);
                throw new Error(`ERROR`);
           }
       });
   });
}