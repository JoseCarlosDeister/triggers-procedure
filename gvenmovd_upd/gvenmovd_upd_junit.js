/**
 * Since: 20-07-2021
 *
 *  Verificar TRIGGER :  gvenmovd_upd
 *  Trigger insert for gvenmovd
 *  Test positivo:
 *      - Foto gvenmovd
 *      - Ejecución y foto al finalizar ejecución.
 *        gvenmovd
 *  Test negativo:
 *
 */
 
 const mArrTests = [
    {
        test_name    : 'Test1 : Desactivar la opcion copiar lineas de la tipologia de albaranes',
        codigo       : 'ABPE',
        copori       : 'N',
        actori       : 'N'
    },
];

const mArrTestsError = [
];

for (let mRowTest of mArrTests){
    test(`Initial snapshot. ${mRowTest.test_name}`, (test_unit) => {
        Ax.jsunit.assertResultSet(test_unit.snapshot, [
            {
                tableName: 'gvenmovd',
                columns  : `gvenmovd.codigo, gvenmovd.nomdoc,   gvenmovd.movalb, gvenmovd.codser,
                            gvenmovd.ctaori, gvenmovd.indlot,   gvenmovd.abcven, gvenmovd.abccon, 
                            gvenmovd.tipmov, gvenmovd.proubi,   gvenmovd.terstk, gvenmovd.movadj, 
                            gvenmovd.abono,  gvenmovd.cominv,   gvenmovd.datcon, gvenmovd.valcat, 
                            gvenmovd.impdoc, gvenmovd.obj_print,gvenmovd.tabori, gvenmovd.actori,
                            gvenmovd.copori, gvenmovd.getacu, gvenmovd.tabdes, gvenmovd.docdes`,
                where    : `codigo = '${mRowTest.codigo}'`
            }  
        ]);
    });

    test(`Execute process. ${mRowTest.test_name}`, (test_unit) => {

        Ax.db.update("gvenmovd",
            {
                copori : mRowTest.copori,
                actori : mRowTest.actori
            },
            {
                codigo : mRowTest.codigo
            }
        );

        Ax.jsunit.assertResultSet(test_unit.snapshot, [
            {
                tableName: 'gvenmovd',
                columns  : `gvenmovd.codigo, gvenmovd.nomdoc,   gvenmovd.movalb, gvenmovd.codser,
                            gvenmovd.ctaori, gvenmovd.indlot,   gvenmovd.abcven, gvenmovd.abccon, 
                            gvenmovd.tipmov, gvenmovd.proubi,   gvenmovd.terstk, gvenmovd.movadj, 
                            gvenmovd.abono,  gvenmovd.cominv,   gvenmovd.datcon, gvenmovd.valcat, 
                            gvenmovd.impdoc, gvenmovd.obj_print,gvenmovd.tabori, gvenmovd.actori,
                            gvenmovd.copori, gvenmovd.getacu, gvenmovd.tabdes, gvenmovd.docdes`,
                where    : `codigo = '${mRowTest.codigo}'`
            } 
        ]);
    });
 }


/** Iteració tests negatius:
 * Aun no se declaro los parametros de entrada del test de error
 */
for(let mRowTestError of mArrTestsError) {

    test(`Expected errors ${mRowTestError.test_name}`, (test_unit) => {
        /**
        * Try, may throw an error for all cases in mSqlCondErr
        */
       Ax.jsunit.assertThrows('ERROR', () => {

           try {

            Ax.db.update("gvenmovd",
                {
                    copori : mRowTestError.copori,
                    actori : mRowTestError.actori
                },
                {
                    codigo : mRowTestError.codigo
                }
            );
           } catch(e) {
               Ax.jsunit.assertEquals(test_unit.snapshot, e.message);
               throw new Error(`ERROR`);
           }
       });
   });
}