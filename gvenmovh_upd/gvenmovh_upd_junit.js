/**
 * Since: 15-07-2021
 *
 *  Verificar TRIGGER :  gvenmovh_upd
 *  Trigger update gvenmovh
 * 
 *  Test positivo:
 *      - Foto : gvenmovh
 *      - Ejecución y foto al finalizar ejecución: 
 *        gvenmovh
 * 
 *  Test negativo:
 *       No hay test a realizar
 */

 const mArrTests = [
    {
        test_name : 'Test 1: Actualizar nota de la expedicion de ventas',
        cabid     : 427,
        coment    : 'Notas de expedicion 427'
    }
];

const mArrTestsError = [
];

for (let mRowTest of mArrTests){

    test(`Initial snapshot. ${mRowTest.test_name}`, (test_unit) => {

        Ax.jsunit.assertResultSet(test_unit.snapshot, [
            {
                tableName: 'gvenmovh',
                columns  : `tipdoc, empcode, delega, depart, almdes, fecmov, docser, coment`,
                where    : `gvenmovh.cabid = ${mRowTest.cabid}`
            }
        ]);
    });

    test(`Execute process. ${mRowTest.test_name}`, (test_unit) => {

        Ax.db.update("gvenmovh", 
            {
                coment: mRowTest.coment
            },
            {
                cabid : mRowTest.cabid
            }
        );

        Ax.jsunit.assertResultSet(test_unit.snapshot, [
            {
                tableName: 'gvenmovh',
                columns  : `tipdoc, empcode, delega, depart, almdes, fecmov, docser, coment`,
                where    : `gvenmovh.cabid = ${mRowTest.cabid}`
            }
        ]);
    });

 }
 /**
 * Test Negativo:
 */
for(let mRowTestError of mArrTestsError) {

    test(`Expected errors ${mRowTestError.test_name}`, (test_unit) => {
        /**
        * Try, may throw an error for all cases in mSqlCondErr
        */
       Ax.jsunit.assertThrows('ERROR', () => {
            try {
                Ax.db.update("gvenmovh", 
                    {
                        coment: mRowTestError.coment
                    },
                    {
                        cabid : mRowTestError.cabid
                    }
                );
            }
            catch(e) {
                Ax.jsunit.assertEquals(test_unit.snapshot, e.message);
                throw new Error(`ERROR`);
           }
       });
   });
}