/**
 * Since: 19-07-2021
 *
 *  Verificar TRIGGER :  gvenmovh_note_ins
 *  Trigger update gvenmovh_note
 * 
 *  Test positivo:
 *      - Foto : gvenmovh_note
 *      - Ejecución y foto al finalizar ejecución: 
 *        gvenmovh_note
 * 
 *  Test negativo:
 *       No hay test a realizar
 */

 const mArrTests = [
    {
        test_name : 'Test 1: Insertar nota de cabecera expedicion de ventas',
        cabid     : 660,
        linid     : null,
        tipnot    : 'AV',
        orden     :  2,
        data      : 'Insert Nota cabecera Nro 2'
    }
];

const mArrTestsError = [
];

for (let mRowTest of mArrTests){
    
    test(`Initial snapshot. ${mRowTest.test_name}`, (test_unit) => {

        Ax.jsunit.assertResultSet(test_unit.snapshot, [
            {
                tableName: 'gvenmovh_note',
                columns  : `cabid, linid, tipnot, orden, data`,
                where    : `cabid  = ${mRowTest.cabid}    AND
                            linid  IS NULL   AND
                            tipnot = '${mRowTest.tipnot}' AND
                            orden  = ${mRowTest.orden}`
            }
        ]);
    });

    test(`Execute process. ${mRowTest.test_name}`, (test_unit) => {

        Ax.db.insert("gvenmovh_note", 
            {
                data: mRowTest.data,
                cabid  :  mRowTest.cabid,
                linid  :  mRowTest.linid,
                tipnot :  mRowTest.tipnot,
                orden  :  mRowTest.orden
            }
        );

        Ax.jsunit.assertResultSet(test_unit.snapshot, [
            {
                tableName: 'gvenmovh_note',
                columns  : `cabid, linid, tipnot, orden, data`,
                where    : `cabid  = ${mRowTest.cabid}  AND
                            linid  IS NULL AND
                            tipnot = '${mRowTest.tipnot}' AND
                            orden  = ${mRowTest.orden}`
            }
        ]);
    });

 }
 /**
 * Test Negativo:
 */
for(let mRowTestError of mArrTestsError) {
    
    test(`Expected errors ${mRowTestError.test_name}`, (test_unit) => {
        /**
        * Try, may throw an error for all cases in mSqlCondErr
        */
        
       Ax.jsunit.assertThrows('ERROR', () => {
            try {
                Ax.db.insert("gvenmovh_note", 
                    {
                        data: mRowTestError.data,
                        cabid  :  mRowTestError.cabid,
                        linid  :  mRowTestError.linid,
                        tipnot :  mRowTestError.tipnot,
                        orden  :  mRowTestError.orden,
                    }
                );
            }
            catch(e) {
                Ax.jsunit.assertEquals(test_unit.snapshot, e.message);
                throw new Error(`ERROR`);
           }
       });
   });
}