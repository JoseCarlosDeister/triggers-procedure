/**
 * Since: 15-07-2021
 *
 *  Verificar TRIGGER :  gvenmovl_datc_ins
 *  Trigger delete for gvenmovl_datc
 * 
 *  Test positivo:
 *      - Foto : gvenmovl_datc, gvenmovl
 *      - Ejecución y foto al finalizar ejecución: 
 *        gvenmovl_datc, gvenmovl
 * 
 *  Test negativo:
 *       No hay test a realizar
 */

 const mArrTests = [
    {
        test_name : 'Test 1: Insertar un registro a datos contables',
        linid     : 3204,
        proyec    : '09',
        seccio    : '0',
        ctacon    : '',
        ctaaux    : '0',
        ctaexp    : 'L01',
        centro    : 'c02',
        codpre    : 'P19-000004',
        porcen    : 100.00000,
        import    : 10,
        codpar    : '' 
    }
];

const mArrTestsError = [
];

for (let mRowTest of mArrTests){

    test(`Initial snapshot. ${mRowTest.test_name}`, (test_unit) => {

        Ax.jsunit.assertResultSet(test_unit.snapshot, [
            {
                tableName: 'gvenmovl_datc',
                columns  : `proyec, seccio, ctacon, ctaaux, ctaexp,
                            centro, porcen, import, codpre, codpar`,
                where    : `gvenmovl_datc.linid  = ${mRowTest.linid}    AND
                            gvenmovl_datc.proyec = '${mRowTest.proyec}' AND
                            gvenmovl_datc.seccio = '${mRowTest.seccio}' AND
                            gvenmovl_datc.ctacon = '${mRowTest.ctacon}' AND
                            gvenmovl_datc.ctaaux = '${mRowTest.ctaaux}' AND
                            gvenmovl_datc.ctaexp = '${mRowTest.ctaexp}' AND
                            gvenmovl_datc.centro = '${mRowTest.centro}' AND
                            gvenmovl_datc.codpre = '${mRowTest.codpre}' AND
                            gvenmovl_datc.codpar = '${mRowTest.codpar}'`
            },
            {
                tableName: 'gvenmovl',
                columns  : `orden, codart, varstk, desvar, numlot, canmov, canalt, udmven, udmalt,
                            estlin, errlin, wkflin, impcos, cosmed, terdep, ubiori, ubides, precio, 
                            dtolin, pretar, dtotar, canabo, canfac`,
                where    : `gvenmovl.linid = ${mRowTest.linid}`
            }
        ]);
    });

    test(`Execute process. ${mRowTest.test_name}`, (test_unit) => {

        Ax.db.insert("gvenmovl_datc", 
            {
                linid : mRowTest.linid,
                proyec: mRowTest.proyec,
                seccio: mRowTest.seccio,
                ctacon: mRowTest.ctacon,
                ctaaux: mRowTest.ctaaux,
                ctaexp: mRowTest.ctaexp,
                centro: mRowTest.centro,
                codpre: mRowTest.codpre,
                codpar: mRowTest.codpar,
                import: mRowTest.import
            }
        );

        Ax.jsunit.assertResultSet(test_unit.snapshot, [
            {
                tableName: 'gvenmovl_datc',
                columns  : `proyec, seccio, ctacon, ctaaux, ctaexp,
                            centro, porcen, import, codpre, codpar`,
                where    : `gvenmovl_datc.linid  =  ${mRowTest.linid}   AND
                            gvenmovl_datc.proyec = '${mRowTest.proyec}' AND
                            gvenmovl_datc.seccio = '${mRowTest.seccio}' AND
                            gvenmovl_datc.ctacon = '${mRowTest.ctacon}' AND
                            gvenmovl_datc.ctaaux = '${mRowTest.ctaaux}' AND
                            gvenmovl_datc.ctaexp = '${mRowTest.ctaexp}' AND
                            gvenmovl_datc.centro = '${mRowTest.centro}' AND
                            gvenmovl_datc.codpre = '${mRowTest.codpre}' AND
                            gvenmovl_datc.codpar = '${mRowTest.codpar}'`
            },
            {
                tableName: 'gvenmovl',
                columns  : `orden, codart, varstk, desvar, numlot, canmov, canalt, udmven, udmalt,
                            estlin, errlin, wkflin, impcos, cosmed, terdep, ubiori, ubides, precio, 
                            dtolin, pretar, dtotar, canabo, canfac`,
                where    : `gvenmovl.linid = ${mRowTest.linid}`
            }
        ]);
    });

 }
 /**
 * Test Negativo:
 */
for(let mRowTestError of mArrTestsError) {

    test(`Expected errors ${mRowTestError.test_name}`, (test_unit) => {
        /**
        * Try, may throw an error for all cases in mSqlCondErr
        */
       Ax.jsunit.assertThrows('ERROR', () => {
            try {
                Ax.db.insert("gvenmovl_datc", 
                    {
                        linid : mRowTestError.linid,
                        proyec: mRowTestError.proyec,
                        seccio: mRowTestError.seccio,
                        ctacon: mRowTestError.ctacon,
                        ctaaux: mRowTestError.ctaaux,
                        ctaexp: mRowTestError.ctaexp,
                        centro: mRowTestError.centro,
                        codpre: mRowTestError.codpre,
                        codpar: mRowTestError.codpar
                    }
                );
            }
            catch(e) {
                Ax.jsunit.assertEquals(test_unit.snapshot, e.message);
                throw new Error(`ERROR`);
           }
       });
   });
}