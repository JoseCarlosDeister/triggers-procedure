/**
 * Since: 29-04-2020
 *
 *  Verificar TRIGGER :  gvenmovh_ins
 *  TRigger insert for gvenmovh
 *  Test positivo:
 *      - Foto gvenmovh
 *      - Ejecución y foto al finalizar ejecución.
 *        gvenmovh
 *  Test negativo:
 *
 */
 
 const mArrTests = [
    {
        // Insert de cabecera de albarán de ventas. 
        test_name        :  'T1. Insert cabecera albaran',
        cabid            :  0,
        tipdoc           :  'AOA', 
        empcode          :  'TRI',
        delega           :  '08',
        depart           :  '81',
        almori           :  'TRI08',
        almdes           :  null,
        fecmov           :  new Ax.sql.Date(2021,02,07),
        fecent           :  null,
        docser           :  'DOCSERTEST001',
        tercer           :  '00036',
        tipdir           :  '0',
        divisa           :  'EUR',
        cambio           :  1,
        impres           :  'N',
        estcab           :  'V',
        errcab           :  0,
        wkfcab           :  null,
        coment           :  'Comentario test',
        tipefe           :  'EF',
        frmpag           :  '30D',
        terenv           :  '00036',
        direnv           :  '0',
        terfac           :  '00036',
        dirfac           :  '0',
        agente           :  null,
        climos           :  null,
        nommos           :  'CUYLÁS, SL',
        ciftyp           :  1,
        cifiss           :  'ES',
        cif              :  'B25017708',
        direcc           :  'Vía Augusta 37',
        poblac           :  'Barcelona',
        codnac           :  'ESP',
        nomnac           :  'España',
        codpos           :  '08006',
        codprv           :  '08',
        nomprv           :  'Barcelona',
        telef1           :  '932 172 190',
        telef2           :  null,
        fax              :  '932 188 126',
        email            :  'INFO@CUYLAS.COM',
        refter           :  null,
        transp           :  null,
        dirtra           :  null,
        medtra           :  null,
        tiptra           :  null,
        numbul           :  0,
        terexp           :  null,
        direxp           :  null,
        docori           :  null,
        oriaux           :  null,
        albh_gross       :  100,
        imptot           :  100,
        impant           :  0,
        impant_ntax      :  null,
        zimemp           :  'ESCO',
        zimter           :  'ESCO',
        codpre           :  null,
        codpar           :  null,
        numexp           :  null,
        conten           :  null,
        clasif           :  null,
        dtogen           :  0,
        dtopp            :  0,
        porgar           :  0,
        bancob           :  null,
        valor            :  null,
        portes           :  'D',
        fcontm           :  null,
        fconta           :  null,
        valstk           :  'N',
        movedi           :  0,
        movest           :  0,
        movhis           :  0,
        indmod           :  'S',
        comisi           :  0,
        auxchr1          :  null,
        auxchr2          :  null,
        auxchr3          :  null,
        auxchr4          :  null,
        auxchr5          :  null,
        auxnum1          :  null,
        auxnum2          :  null,
        auxnum3          :  null,
        auxnum4          :  null,
        auxnum5          :  null
    }
];

const mArrTestsError = [
];

for (let mRowTest of mArrTests){
    test(`Initial snapshot. ${mRowTest.test_name}`, (test_unit) => {
        Ax.jsunit.assertResultSet(test_unit.snapshot, [
            {
                tableName: 'gvenmovh',
                columns  : `*`,
                where    : `cabid = ${mRowTest.cabid}`
            }  
        ]);
    });

    test(`Execute process. ${mRowTest.test_name}`, (test_unit) => {
        
        var mIntCabid = Ax.db.insert("gvenmovh", mRowTest).getSerial();

        Ax.jsunit.assertResultSet(test_unit.snapshot, [
            {
                tableName: 'gvenmovh',
                columns  : `*`,
                where    : `cabid = ${mIntCabid}`
            }
        ]);
    });
 }


/** Iteració tests negatius:
 * Aun no se declaro los parametros de entrada del test de error
 */
for(let mRowTestError of mArrTestsError) {

    test(`Expected errors ${mRowTestError.test_name}`, (test_unit) => {
        /**
        * Try, may throw an error for all cases in mSqlCondErr
        */
       Ax.jsunit.assertThrows('ERROR', () => {

           try {

                Ax.db.insert("gvenmovh", mRowTestError);
           } catch(e) {
               Ax.jsunit.assertEquals(test_unit.snapshot, e.message);
               throw new Error(`ERROR`);
           }
       });
   });
}