/**
 * Since: 20-07-2021
 *
 *  Verificar TRIGGER :  gvenfacl_dtll_upd
 *  Trigger delete for gvenmovl_datc
 * 
 *  Test positivo:
 *      - Foto : gvenfacl_dtll
 *      - Ejecución y foto al finalizar ejecución: 
 *        gvenfacl_dtll
 * 
 *  Test negativo:
 *       No hay test a realizar
 */

 const mArrTests = [
    {
        test_name : 'Test 1: Actualizar lineas de descuentos de las lineas de facturas ',
        linid     : 4596,
        orden     : 8,
        valor     : -10
    }
];

const mArrTestsError = [
];

for (let mRowTest of mArrTests){

    test(`Initial snapshot. ${mRowTest.test_name}`, (test_unit) => {

        Ax.jsunit.assertResultSet(test_unit.snapshot, [
            {
                tableName: 'gvenfacl_dtll',
                columns  : `linid,   orden,  coddto, valor,  formul,
                            tipdto,  bascal, aplfac, aplrap, aplaux1,
                            aplaux2, aplrea, canliq,`,
                where    : `linid = ${mRowTest.linid} AND
                            orden = ${mRowTest.orden}`
            }
        ]);
    });

    test(`Execute process. ${mRowTest.test_name}`, (test_unit) => {

        Ax.db.update("gvenfacl_dtll",
            {
                valor : mRowTest.valor
            },
            {
                linid : mRowTest.linid,
                orden : mRowTest.orden
            }
        );

        Ax.jsunit.assertResultSet(test_unit.snapshot, [
            {
                tableName: 'gvenfacl_dtll',
                columns  : `linid,   orden,  coddto, valor,  formul,
                            tipdto,  bascal, aplfac, aplrap, aplaux1,
                            aplaux2, aplrea, canliq,`,
                where    : `linid = ${mRowTest.linid} AND
                            orden = ${mRowTest.orden}`
            }
        ]);
    });

 }
 /**
 * Test Negativo:
 */
for(let mRowTestError of mArrTestsError) {

    test(`Expected errors ${mRowTestError.test_name}`, (test_unit) => {
        /**
        * Try, may throw an error for all cases in mSqlCondErr
        */
       Ax.jsunit.assertThrows('ERROR', () => {
            try {
                Ax.db.update("gvenfacl_dtll",
                    {
                        valor : mRowTestError.valor
                    },
                    {
                        linid : mRowTestError.linid,
                        orden : mRowTestError.orden
                    }
                );
            }
            catch(e) {
                Ax.jsunit.assertEquals(test_unit.snapshot, e.message);
                throw new Error(`ERROR`);
           }
       });
   });
}