/**
 * Since: 20-07-2021
 *
 *  Verificar TRIGGER :  gvenmovd_ins
 *  TRigger insert for gvenmovd
 *  Test positivo:
 *      - Foto gvenmovd
 *      - Ejecución y foto al finalizar ejecución.
 *        gvenmovd
 *  Test negativo:
 *
 */
 
 const mArrTests = [
    {
        // Insert de cabecera de albarán de ventas. 
        test_name        : 'Test 1: Insertar una tipologia de albaranes',
        codigo           : 'ABTEST',
        nomdoc           : 'ALBARÁN ABONO TEST',
        movalb           : 'M',
        codser           : 'AV',
        estado           : 'A',
        ctaori           : 'NSAL',
        ctades           : 'DISP',
        indlot           : 'A',
        tipmov           : 'E',
        consum           : 'N',
        abcven           : 0,
        abccon           : 0,
        proubi           : 'N',
        terstk           : 'N',
        movadj           : 'N',
        concal           : 0,
        abono            : 'S',
        cominv           : 'N',
        datcon           : 'S',
        valcat           : 'N',
        impdoc           : 'N',
        getacu           : 0,
        valinv           : 'D',
        valord           : 0,
        valdoc           : 'T',
        indrie           : 'N',
        evorie           : 'E',
        pretar           : 'N',
        aplcon           : 0,
        acttes           : 'N',
        dtoesp           : 'S',
        promoc           : 0,
        ivainc           : 0,
        indant           : 'N',
        indest           : 0,
        indhis           : 0,
        hide_varlog      : 0,
        hide_numlot      : 1,
        hide_terdep      : 1,
        hide_codubi      : 1,
        hide_impcos      : 1,
        hide_orden       : 1,
        hide_indmod      : 1,
        hide_udmven      : 0,
        hide_udmalt      : 1,
        hide_regalo      : 1,
        hide_canabo      : 1,
        hide_canfac      : 1,
        hide_canref      : 1,
        hide_agente      : 1,
        hide_codbar      : 1,
        hide_refcli      : 1,
        hide_descli      : 1,
        hide_tab_gexpimpo: 1,
        hide_tab_ctergest: 1,
        user_created     : Ax.db.getUser(),
        date_created     : new Ax.sql.Date(),
        user_updated     : Ax.db.getUser(),
        date_updated     : new Ax.sql.Date()
    },
];

const mArrTestsError = [
];

for (let mRowTest of mArrTests){
    test(`Initial snapshot. ${mRowTest.test_name}`, (test_unit) => {
        Ax.jsunit.assertResultSet(test_unit.snapshot, [
            {
                tableName: 'gvenmovd',
                columns  : `*`,
                where    : `codigo = '${mRowTest.codigo}'`
            }  
        ]);
    });

    test(`Execute process. ${mRowTest.test_name}`, (test_unit) => {
        
        Ax.db.insert("gvenmovd", mRowTest);

        Ax.jsunit.assertResultSet(test_unit.snapshot, [
            {
                tableName: 'gvenmovd',
                columns  : `*`,
                where    : `codigo = '${mRowTest.codigo}'`
            }
        ]);
    });
 }


/** Iteració tests negatius:
 * Aun no se declaro los parametros de entrada del test de error
 */
for(let mRowTestError of mArrTestsError) {

    test(`Expected errors ${mRowTestError.test_name}`, (test_unit) => {
        /**
        * Try, may throw an error for all cases in mSqlCondErr
        */
       Ax.jsunit.assertThrows('ERROR', () => {

           try {

                Ax.db.insert("gvenmovd", mRowTestError);
           } catch(e) {
               Ax.jsunit.assertEquals(test_unit.snapshot, e.message);
               throw new Error(`ERROR`);
           }
       });
   });
}