/**
 * Since: 20-07-2021
 *
 *  Verificar TRIGGER :  gvenfacl_datc_ins
 *  TRigger insert for gvenfacl_datc
 *  Test positivo:
 *      - Foto gvenfacl_datc
 *      - Ejecución y foto al finalizar ejecución.
 *        gvenfacl_datc
 *  Test negativo:
 *
 */
 
 const mArrTests = [
    {
        test_name   : 'Test 1: Insertar datos contables de la linea de facturas',
        linid       : 4596,
        proyec      : '0',
        seccio      : '03',
        ctacon      : '00000.00000',
        ctaaux      : '0',
        ctaexp      : 'L01',
        centro      : 'C01',
        porcen      : 100,
        import      : 94.05,
        codpre      : 'P18-000002',
        codpar      : 'P001'
    },
];

const mArrTestsError = [
];

for (let mRowTest of mArrTests){
    test(`Initial snapshot. ${mRowTest.test_name}`, (test_unit) => {
        Ax.jsunit.assertResultSet(test_unit.snapshot, [
            {
                tableName: 'gvenfacl_datc',
                columns  : `*`,
                where    : `linid  =  ${mRowTest.linid}   AND
                            proyec = '${mRowTest.proyec}' AND
                            seccio = '${mRowTest.seccio}' AND
                            ctacon = '${mRowTest.ctacon}' AND
                            ctaaux = '${mRowTest.ctaaux}' AND
                            ctaexp = '${mRowTest.ctaexp}' AND
                            centro = '${mRowTest.centro}' AND
                            codpre = '${mRowTest.codpre}' AND
                            codpar = '${mRowTest.codpar}'`
           }  
        ]);
    });

    test(`Execute process. ${mRowTest.test_name}`, (test_unit) => {
        
        Ax.db.insert("gvenfacl_datc", mRowTest);

        Ax.jsunit.assertResultSet(test_unit.snapshot, [
            {
                tableName: 'gvenfacl_datc',
                columns  : `*`,
                where    : `linid  =  ${mRowTest.linid}   AND
                            proyec = '${mRowTest.proyec}' AND
                            seccio = '${mRowTest.seccio}' AND
                            ctacon = '${mRowTest.ctacon}' AND
                            ctaaux = '${mRowTest.ctaaux}' AND
                            ctaexp = '${mRowTest.ctaexp}' AND
                            centro = '${mRowTest.centro}' AND
                            codpre = '${mRowTest.codpre}' AND
                            codpar = '${mRowTest.codpar}'`
           }  
        ]);
    });
 }


/** Iteració tests negatius:
 * Aun no se declaro los parametros de entrada del test de error
 */
for(let mRowTestError of mArrTestsError) {

    test(`Expected errors ${mRowTestError.test_name}`, (test_unit) => {
        /**
        * Try, may throw an error for all cases in mSqlCondErr
        */
       Ax.jsunit.assertThrows('ERROR', () => {

           try {

                Ax.db.insert("gvenfacl_datc", mRowTestError);
           } catch(e) {
               Ax.jsunit.assertEquals(test_unit.snapshot, e.message);
               throw new Error(`ERROR`);
           }
       });
   });
}